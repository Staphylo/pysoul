#!/usr/bin/env python3

# flack8 for syntastic
#

from pysoul.config import config
from pysoul.netsoul import netsoul
from pysoul.pid import pid
from pysoul.log import log
import signal
import time
import sys
import os

CFGPATH = "./pysoul.ini"
DEBUG   = True
p = None

def handler(signum, frame):
    log("exit required by user", "user")
    if p:
        p.remove()
    exit(0)

def daemonize():
    if os.fork():
        os._exit(0)
    os.setsid()
    os.chdir("/")
    fd = os.open("/dev/null", os.O_RDWR)
    os.dup2(fd, 0)
    os.dup2(fd, 1)
    os.dup2(fd, 2)

def main():
    global p

    signal.signal(signal.SIGINT, handler)
    cfg = config(CFGPATH)
    if not cfg.load():
        return
    ns = netsoul(bool(cfg.getbool("pysoul.debug")))
    ns.setlocation(cfg.get("pysoul.location"))
    ns.setdata(cfg.get("pysoul.data"))
    try:
        ns.connect(cfg.get("pysoul.server"), cfg.get("pysoul.port"))
    except Exception as e:
        log("network unreachable", "main")
        sys.exit(1)
    if cfg.getbool("pysoul.singleton"):
        p = pid(cfg.get("pysoul.pidfile"))
        if not p.doit():
            return
    if ns.auth(cfg.get("pysoul.login"), cfg.get("pysoul.password")):
        ns.state("actif")
        if cfg.getbool("pysoul.daemonize"):
            daemonize()
        while True:
            ns.loop()
    ns.close()
    if p:
        p.remove()

if __name__ == '__main__':
    main()
