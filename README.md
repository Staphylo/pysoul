PySOUL
======

It's a small netsoul client.
There are 2 frontends to this client:
 - a simple textual one that only connect you to netsoul (pysoul-cli.py)
 - an interactive client in ncurses (pysoul-curses.py)
It doesn't depend on any external libs so it can be run everywere.

Configuration:
==============

Use the configuration file from the doc folder and put it just near main.py
Don't forget to add your personnal information.

Requirements:
=============

Python 3.0 >=

Authors:
========

Samuel *Staphylo* Angebault <angeba_s@epita.fr>

License:
========

Beerware
