# Managing pid file

from pysoul.log import log
import os

# move it as configuration variable
PIDPATH = "~/.pysoul.pid"

class pid:
    path = None
    def __init__(self, path):
        self.path = os.path.expanduser(path)

    def doit(self):
        if self.check():
            log("file exists (%s), maybe pysoul is already running (%s)" %
                (self.path, self.read().rstrip()))
            return False
        self.write()
        return True

    def check(self):
        if os.access(self.path, os.F_OK):
            return True
        return False

    def write(self):
        with open(self.path, "w") as f:
            f.write("{}\n".format(os.getpid()))
        log("written in file %s" % self.path)

    def read(self):
        with open(self.path, "r") as f:
            return f.read()

    def remove(self):
        if self.check():
            log("file removed")
            os.remove(self.path)
