
from time import strftime, gmtime
import inspect

# do a logfile

def caller_module():
    stack = inspect.stack()
    parentframe = stack[2][0]
    return parentframe.f_locals['self'].__class__.__name__

def log(msg, module=''):
    if len(module) == 0:
        module = caller_module()
    print("[{}] {}: {}".format(strftime("%H:%M:%S"), module, msg))
