
from pysoul.log import log
import configparser
import os

def str2bool(string):
    if string == "True" or string == "true":
        return True
    return False

class config:
    data = None
    path = None
    def __init__(self, path):
        self.path = path

    def load_file(self, path, f):
        try:
            self.data = configparser.ConfigParser()
            self.data.read(path)
            log("configuration file loaded ({})".format(path))
        except Exception as e:
            log("error loading configuration")
            for line in str(e).split('\n'):
                log(line)
            return False
        return True


    def load(self, path=None):
        if not path:
            path = self.path
        if not os.access(path, os.R_OK):
            log("configuration file not found or unreadable ({})".format(path))
            return False
        else:
            with open(path, "r") as f:
                return self.load_file(path, f)
            return False

    def get(self, key):
        if self.data:
            array = key.split('.')
            tmp = self.data
            for k in array:
                tmp = tmp[k]
            return tmp
        return None

    def getbool(self, key):
        return str2bool(self.get(key))

    def getint(self, key):
        return int(self.get(key))

    def getfloat(self, key):
        return float(self.get(key))

