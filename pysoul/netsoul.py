
from pysoul.log import log

from time import strftime
import socket
import hashlib
import urllib.parse

class netsoul:
    sock = None
    ip = None
    port = None
    login = None
    password = None
    location = ""
    data = ""
    debug = False
    def __init__(self, debug=False):
        self.debug = debug

    def connect(self, ip, port):
        log("connecting...")
        self.ip = ip
        self.port = port
        self.sock = socket.create_connection((ip, port))
        if self.sock:
            log("connected")
            return True
        else:
            log("connection error")
            return False

    def auth(self, login, password):
        if not self.sock:
            log("cannot auth without being connected")
            return False
        log("authenticating as {}...".format(login))
        salut = self.recv()
        self.send("auth_ag ext_user none none")
        resp = self.recv()
        if resp != "rep 002 -- cmd end":
            log("unable to authentificate - server refuses")
            return False
        array = str(salut).split(' ')
        query = "{}-{}/{}{}".format(array[2], array[3], array[4], password)
        digest = hashlib.md5(query.encode('ascii')).hexdigest()
        query = "ext_user_log {} {} {} {}".format(login, digest,
                self.location, self.data)
        self.send(query)
        resp = self.recv()
        if resp != "rep 002 -- cmd end":
            log("unable to authentificate - wrong login / password")
            return False
        else:
            log("authenticated")
            self.login = login
            self.password = password
            return True

    def loop(self):
        while True:
            res = self.recv()
            array = res.split(' ')
            if array[0] == "ping":
                self.send(res)
            elif array[0] == "user_cmd":
                self.user_cmd(res, array)
            else:
                log("unknown: {}".format(res))

    def user_cmd(self, res, array):
        if array[3] == "msg":
            info = array[1].split(':')
            print("{}: {}".format(info[3],
                urllib.parse.unquote_plus(array[4])))
        else:
            log("user_cmd: {}".format(res))

    def state(self, state):
        # actif away connection idle lock server none
        self.send("user_cmd state {}:{}".format(state, strftime("%s")))

    def format_logins(self, logins):
        if len(logins) == 0:
            raise Exception
        if type(logins) is list:
            return "\{{}\}".format(logins.join(','))
        if type(logins) is str:
            return logins
        raise Exception

    def watch_log_users(self, logins):
        self.send("watch_log_user {}".format(self.format_logins(logins)))

    def list_users(self, logins):
        self.send("user_cmd list_users {}".format(self.format_logins(logins)))

    def who(self, login):
        self.send("user_cmd who {}".format(self.format_logins(users)))

    def message(self, login, message):
        if len(login) == 0 or len(message) == 0:
            return
        message = urllib.parse.quotes_plus(message)
        self.send("user_cmd msg_user {} msg {}".format(login, message))

    def reconnect(self):
        if not self.ip or not self.port:
            return False
        self.connect(self.ip, self.port)
        self.auth(self.login, self.password)

    def send(self, msg):
        if self.sock:
            if self.debug:
                log("send >> {}".format(msg))
            msg += "\n"
            self.sock.send(bytearray(msg, 'ascii'))

    def recv(self):
        if self.sock:
            msg = self.sock.recv(1024)
            if len(msg) == 0:
                raise Exception;
            msg = msg.decode()[:-1]
            if self.debug:
                log("recv << {}".format(msg))
            return msg

    def close(self):
        if self.sock:
            self.send("exit")
            self.sock.close()
            self.sock = None

    def setlocation(self, location=""):
        self.location = urllib.parse.quote_plus(location)

    def setdata(self, data=""):
        self.data = urllib.parse.quote_plus(data)

    def __del__(self):
        self.close()
